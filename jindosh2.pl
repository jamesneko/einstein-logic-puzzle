#!/usr/bin/env perl
# Spoiler Warning version
use warnings;
use strict;
use lib '.';
use EinsteinLogicPuzzle;

observe(qw/name:Winslow name:Marcolla name:Contee name:Natsiou name:Finch col:Red col:White col:Blue col:Green col:Purple from:Fraeport from:Baleton from:Karnaca from:Dabokva from:Dunwall drink:Whiskey drink:Absinthe drink:Rum drink:Beer drink:Wine item:Ring item:Snuff item:Medal item:Diamond item:BirdPendant seat:1 seat:2 seat:3 seat:4 seat:5/); init; set 'name:Finch', 'col:Red'; set 'name:Contee', 'seat:1'; set 'col:White', 'seat:2'; set 'col:Blue', 'drink:Whiskey'; set 'from:Fraeport', 'col:Purple'; set 'name:Natsiou', 'item:Snuff'; set 'from:Baleton', 'item:Medal'; set 'name:Winslow', 'drink:Rum'; set 'from:Dabokva', 'drink:Beer'; set 'seat:3', 'drink:Wine'; set 'name:Marcolla', 'from:Dunwall';
init;

set 'name:Contee', 'col:Purple';
set 'name:Marcolla', 'seat:1';
set 'seat:2', 'col:Green';
set 'col:Blue', 'drink:Beer';
set 'from:Karnaca', 'col:White';
not 'from:Karnaca', 'item:Diamond';

set 'name:Finch', 'item:Snuff';
set 'from:Fraeport', 'item:Medal';

set 'name:Natsiou', 'drink:Whiskey';
set 'from:Baleton', 'drink:Rum';
set 'seat:3', 'drink:Wine';
set 'name:Winslow', 'from:Dabokva';


fill_across;
derive;
fill_across;
derive;
fill_across;
tables;

set 'name:Marcolla', 'col:White';
set 'seat:1', 'drink:Absinthe';

# someone carried ring, when visitor from Dunwall next to her almost spilled her neighbour's absinthe
set 'seat:2', 'from:Dunwall';
set 'seat:2', 'drink:Whiskey';
set 'seat:1', 'item:Ring';

set 'name:Finch', 'from:Baleton';
set 'from:Baleton', 'col:Red';
set 'name:Winslow', 'col:Blue';
set 'name:Contee', 'drink:Wine';
set 'seat:3', 'from:Fraeport';

# blue < red
set 'seat:4', 'col:Blue';
set 'seat:5', 'col:Red';

# woman next to diamond lives in Karnaca
set 'seat:2', 'item:Diamond';
set 'seat:4', 'item:BirdPendant';

fill_across;
derive;
fill_across;
tables;



