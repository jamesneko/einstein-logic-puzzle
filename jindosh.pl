#!/usr/bin/env perl
use warnings;
use strict;
use lib '.';
use EinsteinLogicPuzzle;

# The definitions of what types of info we have:
observe(qw/name:Winslow name:Marcolla name:Contee name:Natsiou name:Finch col:Red col:White col:Blue col:Green col:Purple from:Fraeport from:Baleton from:Karnaca from:Dabokva from:Dunwall drink:Whiskey drink:Absinthe drink:Rum drink:Beer drink:Wine item:Ring item:Snuff item:Medal item:Diamond item:BirdPendant seat:1 seat:2 seat:3 seat:4 seat:5/);
init; 

# Initial bits of data we know directly from the riddle
set 'name:Finch', 'col:Red';
set 'name:Contee', 'seat:1';
set 'col:White', 'seat:2';
set 'col:Blue', 'drink:Whiskey';
set 'from:Fraeport', 'col:Purple';
set 'name:Natsiou', 'item:Snuff';
set 'from:Baleton', 'item:Medal';
set 'name:Winslow', 'drink:Rum';
set 'from:Dabokva', 'drink:Beer';
set 'seat:3', 'drink:Wine';
set 'name:Marcolla', 'from:Dunwall';

# Some initial deductions
set 'name:Contee', 'col:Purple'; set 'name:Contee', 'from:Fraeport'; # Contee must be Purple, is not white, blue, green, or red.
set 'seat:2', 'item:Ring'; # woman next to contee bragging about ring

fill_across;

tables;
derive;
set 'name:Contee', 'drink:Absinthe'; # collapse
fill_across;

tables;
derive;
fill_across;
# Contee's item must be Diamond or BirdPendant.
# Someone else carried a Diamond and when she saw it (Medal), the visitof from Karnaca next to her almost spilled her neighbour's absinthe.
# As we've just learned Contee's drink is Absinthe, means seat 2 is from Karnaca, seat 1 is Diamond
set 'name:Contee', 'item:Diamond';
set 'seat:2', 'from:Karnaca';
derive;
fill_across;

tables;
# which tells us that the lady from Karnaca must be Winslow, the Rum drinker
set 'from:Karnaca', 'name:Winslow';
fill_across;
derive;

tables;
# we've got seat 1 and 2 down, but nothing seat-based is further derivable yet;
# we can, however, derive that the lady from Baleton must be Finch, leaving the lady from Dabokva to be Natsiou.
set 'from:Baleton', 'name:Finch'; set 'from:Dabokva', 'name:Natsiou';
fill_across;
derive;
fill_across;

tables;
# which informs us what the lady from Baleton, Finch drinks:
set 'from:Baleton', 'drink:Wine';
# And who owns the BirdPendant:
set 'name:Marcolla', 'item:BirdPendant';
# and Natsiou's colour must be Green:
set 'name:Natsiou', 'col:Green';
fill_across;
derive;
fill_across;

tables;
set 'name:Marcolla', 'drink:Whiskey';
set 'name:Marcolla', 'col:Blue';
# The lady in blue sat left of someone in green
# at this point, we've got seats 1,2,3 down pat, so Marcolla can only be in seat 4, and seat 5 is green.
set 'name:Marcolla', 'seat:4';
set 'seat:5', 'col:Green';

fill_across;
derive;
fill_across;

tables;
# and we've got it all!
#       : Winslow    Marcolla   Contee Natsiou   Finch
# ----------------------------------------------------
# col   :   White        Blue   Purple   Green     Red
# seat  :       2           4        1       5       3
# item  :    Ring BirdPendant  Diamond   Snuff   Medal
# drink :     Rum     Whiskey Absinthe    Beer    Wine
# from  : Karnaca     Dunwall Fraeport Dabokva Baleton

