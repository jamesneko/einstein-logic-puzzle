# EinsteinLogicPuzzle.pm

Little Perl doodle I wrote to help me work through the classic "Einstein" logic puzzle where you work out who lives in what house etc.

In particular, this one comes with a script - `jindosh.pl` - working through the "Jindosh Riddle" I came across in my Dishonored 2 playthrough. It's apparently randomised, so won't be directly applicable to your game, but edit the starting conditions and you should be fine.

![Example Riddle](riddle.jpg){width=640}
![Screenshot](screenshot.png){width=640}

## To Run

It only needs `List::MoreUtils` above the standard Perl CORE modules. Get cpanminus, run `cpanm --installdeps .` in the dir, run `./jindosh.pl` and you can see it in action.

## Exercise For The Reader

```perl
  if ($to->collapsible) {
    say "  => " . $to->debug . " is now collapsible!";
    # I could make this happen automatically, but wanted to work through it myself a bit.
  }
```

This point could be made 'smart' to just keep going when it finds something that's marked with only one unknown (`?`) and the rest are all negations (`!`). I decided to not fill in this part since I wanted to work through it bit by bit; the rest of the puzzle logic performed by `fill_across` and `derive` is fairly mundane.

I coulda organised the data structures better but this works fine.
