package EinsteinLogicPuzzle;

use v5.16;
use warnings;
use strict;

use Exporter qw/import/;
use List::Util qw/sum/;
use List::MoreUtils qw/any uniq first_index first_value minmax/;
use Term::ANSIColor qw/colorstrip/;

our @EXPORT = qw/TYPES KNOWN observe init set not fill_across derive table tables/;

# Map typename to array of all known values
our %TYPES;

# Compare every type of fact with every other type of fact, effectively as a 5-element array,
# with each slot in $KNOWN{type}->{other-type"}[...] associated with $TYPES{other-type}[i]
our %KNOWN;


sub typename2idx
{
  my ($type, $value) = @_;
  my $idx = first_index { $_ eq $value } @{$TYPES{$type}};
  die "No $type:$value known" if $idx == -1;
  return $idx;
}


sub leftpad
{
  my ($str, $width) = @_;
  my $len = length(colorstrip($str));
  return ' ' x ($width - $len) . $str;
}


# Represents a single cell in our big table, when comparing one type with another type's possible values.
# Contains a superposition of potential type1's values, to express our certainty about it. Should ultimately
# collapse down to a single value from type1.
package Superposition {
  my %colours = ( '?' => 'bold black', '!' => 'bold red', '=' => 'bold green' );
  sub colour_check
  {
    my ($mark) = @_;
    return Term::ANSIColor::colored($mark, $colours{$mark} // 'bold white');
  }

  sub new
  {
    my ($class, %args) = @_;
    my $self = {
      type => $args{type},
      addr => $args{addr}, # e.g. name%col:Red
      derived_from => undef,
      values => { map { $_ => '?' } @{$TYPES{$args{type}}} },  # typename => ?, !, or =
    };
    return bless $self, $class;
  }
  
  sub type
  {
    my ($self) = @_;
    return $self->{type};
  }

  sub addr
  {
    my ($self) = @_;
    return $self->{addr};
  }
  
  sub as_array
  {
    my ($self) = @_;
    return [ map { $self->{values}->{$_} // '?' } @{$TYPES{$self->type}} ];
  }
  
  sub as_string
  {
    my ($self) = @_;
    return $self->determined if $self->determined;
    return join('', map { $self->{values}->{$_} // '?' } @{$TYPES{$self->type}} );
  }

  sub as_ansi
  {
    my ($self) = @_;
    return Term::ANSIColor::colored($self->determined, 'green') if $self->determined && $self->derived_from;
    return Term::ANSIColor::colored($self->determined, 'bold green') if $self->determined;
    return join('', map { colour_check($self->{values}->{$_} // '?') } @{$TYPES{$self->type}} );
  }
  
  sub get
  {
    my ($self, $value) = @_;
    return $self->{values}->{$value} // '?';
  }

  sub set
  {
    my ($self, $value, $probability) = @_;
    return $self->{values}->{$value} = $probability;
  }

  # Mark us as maybe being this value. Shouldn't be needed.
  sub maybe
  {
    my ($self, $value) = @_;
    $self->set($value, '?');
  }

  # Mark us as definitely not being this value.
  sub not
  {
    my ($self, $value) = @_;
    $self->set($value, '!');
  }

  # Mark us as definitely being this value.
  sub certain
  {
    my ($self, $value) = @_;
    die "Attempt to set $self->{addr} ->certain($value), but we already ruled that value out!" if $self->get($value) eq '!';
    foreach my $slotvalue (keys %{$self->{values}}) {
      $self->set($slotvalue, $slotvalue eq $value ? '=' : '!');
    }
    delete $self->{derived_from};
  }

  # Test if we are definitely a value. Returns undef or value name.
  sub determined
  {
    my ($self, $value) = @_;
    return if List::MoreUtils::any { $_ eq '?' } @{$self->as_array};
    return List::MoreUtils::first_value { $self->{values}->{$_} eq '=' } (keys %{$self->{values}});
  }
  
  # True if we can definitely be one value by process of elimination
  sub collapsible
  {
    my ($self) = @_;
    my @uncertain = grep { $_ eq '?' } @{$self->as_array};
    return @uncertain == 1;
  }
  
  sub collapse
  {
    my ($self) = @_;
    my @uncertain = grep { $_ eq '?' } @{$self->as_array};
    die "Asked to collapse " . $self->as_string() . ", but not collapsible!" unless @uncertain == 1;
    foreach my $value (keys %{$self->{values}}) {
      $self->{values}->{$value} = '=' if $self->{values}->{$value} eq '?';
    }
  }
  
  sub derived_from
  {
    my ($self) = @_;
    return $self->{derived_from};
  }
  
  sub add_derived_from
  {
    my ($self, $from) = @_;
    $self->{derived_from} //= {};
    $self->{derived_from}->{$from}++;
  }
  
  sub debug
  {
    my ($self) = @_;
    return $self->addr . "(" . $self->as_ansi . ")";
  }
};


# Build up list of types and values.
# observe('drink:wine') or observe('drink:wine', 'drink:beer', 'drink:blood');
sub observe
{
  foreach my $typeval (@_) {
    my ($type, $value) = split(':', $typeval);
    $TYPES{$type} //= [];
    $TYPES{$type} = [ uniq @{$TYPES{$type}}, $value ];
  }
}


# Once you've observe()d all types and values, zero out the arrays of all relevant permutations.
sub init
{
  foreach my $type1 (keys %TYPES) {
    foreach my $type2 (keys %TYPES) {
      next if $type1 eq $type2;
      
      # make an array that has indexes suitable for the values type2 can take.
      # each cell in this array is a Superposition of potential type1 values.
      $KNOWN{$type1}->{$type2} = [ map { Superposition->new(type => $type1, addr => "$type1\%$type2:$_") } @{$TYPES{$type2}} ];
    }
  }
}


# Store a relationship, and its inverse.
# set('house:red', 'drink:wine')
sub set
{
  my ($thing1, $thing2) = @_;
  my ($type1, $value1) = split(':', $thing1);
  my ($type2, $value2) = split(':', $thing2);
  my $value1idx = typename2idx($type1, $value1);
  my $value2idx = typename2idx($type2, $value2);
  $KNOWN{$type1}->{$type2}->[$value2idx]->certain($value1);
  $KNOWN{$type2}->{$type1}->[$value1idx]->certain($value2);
}


# Cross off a relationship, and its inverse.
# not('house:red', 'drink:beer')
sub not
{
  my ($thing1, $thing2) = @_;
  my ($type1, $value1) = split(':', $thing1);
  my ($type2, $value2) = split(':', $thing2);
  my $value1idx = typename2idx($type1, $value1);
  my $value2idx = typename2idx($type2, $value2);
  
  $KNOWN{$type1}->{$type2}->[$value2idx]->not($value1);
  $KNOWN{$type2}->{$type1}->[$value1idx]->not($value2);
}


# Go over the whole table, if we have any rows that have a certainty in them,
# then we know the other cells in that row are ! that value.
sub fill_across
{
  foreach my $yaxis (keys %TYPES) {
    foreach my $xaxis (keys %TYPES) {
      next if $yaxis eq $xaxis;
      foreach my $dsp (grep { $_->determined } @{$KNOWN{$yaxis}->{$xaxis}}) {
        my $dsp_value = $dsp->determined;
        foreach my $sp (@{$KNOWN{$yaxis}->{$xaxis}}) {
          $sp->not($dsp_value) unless $sp->determined;
        }
      }
    }
  }
}


sub smoosh
{
  my ($from, $to) = @_;
  my $fromdbg = $from->debug;
  my $todbg = $to->debug;
  return if ($from->as_string eq $to->as_string);
  
  my $actions_taken = 0;
  foreach my $pvalue (@{$TYPES{$from->type}}) {
    next if $from->get($pvalue) eq '?';
    next if $from->get($pvalue) eq $to->get($pvalue);
    die "Attempt to assign certainty from $fromdbg onto $todbg but it is already set as not that value." if $from->get($pvalue) eq '=' && $to->get($pvalue) eq '!';
    die "Attempt to assign negative certainty from from $fromdbg onto $todbg but it is already set as certain for that value." if $from->get($pvalue) eq '!' && $to->get($pvalue) eq '=';
    
    $to->set($pvalue, $from->get($pvalue));
    $actions_taken++;
  }
  if ($actions_taken) {
    say "     smooshed from $fromdbg onto $todbg";
    $to->add_derived_from($from);
  }
  if ($to->collapsible) {
    say "  => " . $to->debug . " is now collapsible!";
    # I could make this happen automatically, but wanted to work through it myself a bit.
    $to->collapse();
  }
}


# We might know that name:Contee sits in seat:1, and know that name:Contee wears col:Purple,
# but we should fill in the table so that we also know that seat:1 has a col:Purple occupant.
sub derive
{
  foreach my $yaxis (keys %TYPES) {
    foreach my $xaxis (keys %TYPES) {
      next if $yaxis eq $xaxis;
      for (my $target_column = 0; $target_column < @{$KNOWN{$yaxis}->{$xaxis}}; $target_column++) {
        my $dsp = $KNOWN{$yaxis}->{$xaxis}->[$target_column];
        next unless $dsp->determined;
        say "==== Deriving what we can from " . $dsp->debug() . " ====";
        # yaxis is the type of our Determined value, e.g. yaxis=name, dsp->determined=Contee
        # can also get that from $dsp->type.
        # go to the table that has name as the xaxis, i.e. "all the things we know about people with these names"
        # we can then fill in the column in the original table ($xaxis, seat:1)
        my $source_column = typename2idx($dsp->type, $dsp->determined);
        my @rows = grep { defined } map { $KNOWN{$_}->{$dsp->type} } keys %KNOWN; # list of arrayrefs
        my @deets = map { $_->[$source_column] } @rows; # Just the sp for that vertical slice
        foreach my $deet (@deets) {
          my $target_yaxis = $deet->type;
          next if $target_yaxis eq $xaxis;
          my $equivalent = $KNOWN{$target_yaxis}->{$xaxis}->[$target_column];
          smoosh($deet, $equivalent);
        }
      }
    }
  }
}


sub table
{
  my ($xaxis) = @_;
  my @table;
  push @table, ['', @{$TYPES{$xaxis}}];
  foreach my $yaxis (keys %TYPES) {
    next if $yaxis eq $xaxis;
    push @table, [$yaxis, @{$KNOWN{$yaxis}->{$xaxis}}];
  }
  return ansi(@table);
}


sub tables
{
  say join("\n", map { table $_ } keys %TYPES);
}


sub ansi
{
  my (@table) = @_;
  my @colwidths;
  foreach my $col (0..(scalar(@{$table[0]})-1)) {
    my @widths = map { my $str = ref $_->[$col] ? $_->[$col]->as_string() : $_->[$col]; length($str) } @table;
    my ($min, $max) = minmax @widths;
    push @colwidths, $max;
  }
  my $leftwidth = shift @colwidths;
  my $dashline = '-' x ($leftwidth + 2 + @colwidths + sum(@colwidths));
  my $s;
  foreach my $row (@table) {
    my @rowitems = @$row;
    $s .= sprintf("%-${leftwidth}s :", shift @rowitems);
    for (my $i = 0; $i < @rowitems; $i++) {
      my $it = $rowitems[$i];
      $s .= ' ' . leftpad(ref $it ? $it->as_ansi : $it, $colwidths[$i]);
    }
    $s .= "\n";
    if ($dashline) {
      $s .= "$dashline\n";
      $dashline = undef;
    }
  }
  return $s;
}



1;
